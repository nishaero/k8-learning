Installing monitoring tools Prometheus and Grafana using helm

1. Install Helm 3 in debian using

snap install helm

2. Install prometheus helm repo

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm update

3. Create a new namespace or use an existing one.

kubectl create ns monitoring

4. Install kube-prometheus-stack for installing prometheus and grafana in namespace

helm install prometheus prometheus-community/kube-prometheus-stack  --namespace monitoring

6. To uninstall prometheus, remove or delete the release name we used for installing prometheus from cluster.