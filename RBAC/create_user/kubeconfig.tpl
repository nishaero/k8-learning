apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: $K8_CLUSTER_CA
    server: $K8_CLUSTER_ENDPOINT
  name: $K8_CLUSTER_NAME
users:
- name: $USER
  user:
    client-certificate-data: $K8_CLIENT_CERTIFICATE
    client-key-data: $K8_CLIENT_KEY
contexts:
- context:
    cluster: $K8_CLUSTER_NAME
    user: $USER
  name: $USER-context
current-context: $USER-context