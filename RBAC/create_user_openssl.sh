#!/bin/bash

#script location

_dir_=$(pwd)

echo Enter username!
read USER

# There is no validation being done here
mkdir $_dir_/$USER

cd $_dir_/$USER/

echo creating openssl private key for user $USER

openssl genrsa -out $USER.key 4096

echo creating certificate signing request using private key of $USER

openssl req -new -key $USER.key -out $USER.csr -subj "/CN=$USER"

echo converting csr to base64
export $USER
export K8_BASE_64_CSR=$(cat $USER.csr | base64 | tr -d '\n')

echo $K8_BASE_64_CSR

echo Sending csr for approval

cat $_dir_/create_user/csr.tpl | envsubst | kubectl apply -f -

echo Approve CSR

kubectl certificate approve "$USER-csr"

echo user $USER is created and approved

# Create context for user and set context in kubeconfig

kubectl get csr "$USER-csr" -o jsonpath='{.status.certificate}' | base64 --decode > "$USER.crt"

openssl x509 -in "$USER.crt" -noout -text

openssl x509 -noout -modulus -in "$USER.crt" | openssl md5
openssl rsa -noout -modulus -in "$USER.key" | openssl md5

export K8_CLUSTER_NAME=$(kubectl config view --minify -o jsonpath={.current-context})
# Client certificate
export K8_CLIENT_CERTIFICATE=$(kubectl get csr "$USER-csr" -o jsonpath='{.status.certificate}')
export K8_CLIENT_KEY=$(cat "$USER.key" | base64 | tr -d '\n')
# Cluster Certificate Authority
export K8_CLUSTER_CA=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."certificate-authority-data"')
# API Server endpoint
export K8_CLUSTER_ENDPOINT=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."server"')

cat "$_dir_/create_user/kubeconfig.tpl" | envsubst > "$USER.kubeconfig"

# Adding file to gitignore to not include
# directory with user certificates

cd ../../

echo "/$USER" >> .gitignore
