https://www.freecram.com/question/LinuxFoundation.CKA.v2021-07-16.q22/score-5-exhibit-task-from-the-pod-label-name-cpu-utilizer-find-pods-running-high-cpu-workloads-and

*Upgrade control plane node*
ssh mk8-master-0
k cordon mk8-master-0
kubeadm version

kubeadm upgrade plan

k drain mk8-master-0 --ignore-daemonsets
sudo kubeadm upgrade apply v1.20.1
apt-get update && apt-get install -y kubelet=1.20.1-00 kubectl=1.20.1-00

sudo systemctl daemon-reload
sudo systemctl restart kubelet

k uncordon mk8-master-0

*find pod with high cpu using*

k top -l name=cpu-utilizer -A -o NAME > opt/KUTR00401/KUTR00401.txt

*List nodes that are ready not including tainted and noschedule*

JSONPATH='{range .items[*]}{@.metadata.name}:{range @.status.conditions[*]}{@.type}={@.status};{end}{end}' \
 && kubectl get nodes -o jsonpath="$JSONPATH" | grep "Ready=True" >  /opt/KUSC00402/kusc00402.txt

*monitor logs of pod foo and extract log lines corresponding to unable-to-access-website*

k logs foo | grep unable-to-access-website > /opt/KULM00201/foo

k run kusc00101 --image=nginx --dry-run=client -o yaml > assign-pod-to-node.yml

add nodeSelector: to container spec

*list pv sorted by capacity using only kubectl sort*

k get pv --sort-by=.spec.capacity.storage > /opt/KUCC00102/volume_list

*list all pods that implements service name baz in development namespace*

k describe svc baz -n development

get the selector name and use it to filter the pod

k get po -l name=foo -n development -o NAME >> /opt/KUCC00302/kucc00302.txt

*create pod with volume that is persistant*

use emptyDir: {} which is persistant when no pv or pvc is required

*toubleshoot node in notReady state*

login to node using ssh
get sudo -i
check kubelet
sudo systemctl status kubelet

*kubeadm condigure node as master*

