Use helm to install gitlab runners on kubernetes.
Prerequisites:
* need kubectl configuration to have access to the cluster

Issue the following command while keeping the location of the values.yaml file in the same directory from where the command is issued.

Following the documentation: https://docs.gitlab.com/runner/install/kubernetes.html
Using Helm v3, no tiller

/**
    helm repo add gitlab https://charts.gitlab.io
*/

/**
    helm init
*/

/**
    helm install --namespace <NAMESPACE> gitlab-runner -f <CONFIG_VALUES_FILE(values.yaml)> gitlab/gitlab-runner
*/

Use 'helm upgrade' to update any changes to the gitlab runner config

/**
    helm upgrade --namespace <NAMESPACE> -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
*/

In the values.yaml file, please provide the gitlab runner token (found in gitlab project->settings->CI/CD->expand Runners) and gitlab url as 'https://gitlab.com/'

Your runners must now be installed and visible under Gitlab CI/CD runner settings



To Use Kubernetes executor when running on k8 cluster

add the folloing to the values.yaml file to the gitlab-runners chart

This solution also works for pipeline build errors such as

/*ERROR: Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?*/
Reference: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-kubernetes

runners:
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"

Also the add the following variables to the gitlab-ci pipeline

variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_TLS_VERIFY: 1
    DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"

Also if you encounter the following ERROR or similar when using a dockerfile

/*DOCKERFILEV0 docker parse error unknown flag: --mount*/

Use Docker buildkit 1 and set this as a vailable in the gitlab-ci pipeline

variables:
    DOCKER_BUILDKIT: 1

And add the follwoing in the first line of the dockerfile

# syntax=docker/dockerfile:1

Reference: https://docs.docker.com/engine/reference/builder/